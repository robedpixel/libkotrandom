import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    kotlin("jvm") version "2.1.0"
    application
    id("com.diffplug.spotless") version "7.0.0.BETA1"
    id("se.patrikerdes.use-latest-versions")  version "0.2.18"
    id("com.github.ben-manes.versions") version "0.51.0"
}
//ext.platform = if (osdetector.os == 'osx') 'mac' else if(osdetector.os == 'windows') 'win' else osdetector.os
/*spotless {
    kotlin {
        ktlint()
    }
}*/

java{
    sourceCompatibility = JavaVersion.VERSION_22
    targetCompatibility = JavaVersion.VERSION_21
}

repositories {
    mavenCentral()
}

val provided = configurations.create("provided");

configurations {
    implementation {
        extendsFrom(configurations["provided"])
    }
}

dependencies {
    provided("org.apache.commons:commons-lang3:3.14.0")
    provided("commons-io:commons-io:2.16.1")
    provided("org.jetbrains.kotlin:kotlin-stdlib:2.0.20-Beta1")
    testImplementation(kotlin("test-junit"))
}

tasks.withType<Jar> {
    archiveClassifier.set("SNAPSHOT")
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    manifest {
        attributes["Main-Class"] = "RunnerKt"
    }
    from("jni_lib") {
        into("native")
    }
    from(configurations["provided"].asFileTree.files.map {zipTree(it)})
}


tasks.compileKotlin {
    dependsOn("spotlessApply")
    compilerOptions{
        jvmTarget.set(JvmTarget.JVM_21)
    }
}

tasks.compileTestKotlin {
    compilerOptions{
        jvmTarget.set(JvmTarget.JVM_21)
    }
}

application {
    mainClass.set("RunnerKt")
}
