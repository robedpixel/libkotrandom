import org.apache.commons.io.FileUtils
import java.io.File
import java.io.IOException
import java.lang.foreign.Arena
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

class KotRandomManager private constructor() {
    private var jarPath: String? = null
    private var cl: CustomClassLoader? = null
    private var ca: Class<*>? = null
    private var a: Any? = null
    private var p_init: Method? = null
    private var p_destroy: Method? = null
    private var p_getLong: Method? = null
    private var p_getInt: Method? = null
    private var p_getShort: Method? = null
    private var p_getRNGQuality: Method? = null
    private var allocator: Arena? = null

    init {
        try {
            jarPath = File(
                KotRandom::class.java.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()
            ).getParent()
            allocator = Arena.ofShared()
            cl = CustomClassLoader()
            ca = cl!!.findClass("KotRandom")
            a = ca!!.getDeclaredConstructor(String::class.java, Arena::class.java).newInstance(jarPath, allocator)
            p_init = ca!!.getMethod("init")
            p_destroy = ca!!.getMethod("destroy")
            p_getLong = ca!!.getMethod("nextLong")
            p_getInt = ca!!.getMethod("nextInt")
            p_getShort = ca!!.getMethod("nextShort")
            p_getRNGQuality = ca!!.getMethod("getRNGQuality")
            p_init!!.invoke(a)
            Runtime.getRuntime().addShutdownHook(Thread {
                try {
                    instance!!.close()
                } catch (e: InvocationTargetException) {
                    println(e.message)
                } catch (e: IllegalAccessException) {
                    println(e.message)
                }
            })
        } catch (e: Exception) {
            println(e.message)
        }
    }

    @Synchronized
    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun nextLong(): Long {
        return p_getLong!!.invoke(a) as Long
    }

    @Synchronized
    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun nextInt(): Int {
        return p_getInt!!.invoke(a) as Int
    }

    @Synchronized
    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun nextShort(): Short {
        return p_getShort!!.invoke(a) as Short
    }

    @get:Throws(InvocationTargetException::class, IllegalAccessException::class)
    @get:Synchronized
    val rngQuality: Byte
        get() = p_getRNGQuality!!.invoke(a) as Byte

    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    private fun close() {
        p_destroy!!.invoke(a)
        cl = null
        ca = null
        a = null
        p_init = null
        p_destroy = null
        p_getLong = null
        p_getInt = null
        p_getShort = null
        p_getRNGQuality = null
        allocator!!.close()
        System.gc()
        System.gc()
        val nativeDir = File(jarPath + File.separator + "libjavarandom_native")
        val osDir = File(jarPath + File.separator + "libjavarandom_native" + File.separator + "windows")
        try {
            FileUtils.deleteDirectory(osDir)
            FileUtils.deleteDirectory(nativeDir)
        } catch (e: IOException) {
            println("error cleaning up libjavarandom tempfiles : failure")
        } catch (e: IllegalArgumentException) {
            println("error cleaning up libjavarandom tempfiles : dir does not exist")
        }
        instance = null
    }

    companion object {
        private var instance: KotRandomManager? = null
        private val mutex = Any()

        private fun getInstance(): KotRandomManager {
            var localRef = instance
            if (localRef == null) {
                synchronized(mutex) {
                    localRef = instance
                    if (localRef == null) {
                        localRef = KotRandomManager()
                        instance = localRef
                    }
                }
            }
            return localRef!!
        }

        val randomInstance: KotRandomInstance
            get() = KotRandomInstance(getInstance())
    }
}