import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.SystemUtils
import java.io.File
import java.lang.foreign.*
import java.lang.invoke.MethodHandle

class KotRandom(
    jarPath: String, // Save as HelloJNI.java
    private val allocator: Arena
) {
    var linker: Linker = Linker.nativeLinker()
    lateinit var lib: SymbolLookup
    lateinit var init: MethodHandle
    lateinit var destroy: MethodHandle
    lateinit var nextShort: MethodHandle
    lateinit var nextInt: MethodHandle
    lateinit var nextLong: MethodHandle
    lateinit var getRNGQuality: MethodHandle
    private var pointer: Long = 0

    init {
        try {
            println("jar location: $jarPath")
            if (SystemUtils.IS_OS_WINDOWS) {
                //do stuff if windows
                p_unpackWindowsLibraries(jarPath)
                p_loadWindowsLibraries(jarPath)
            } else if (SystemUtils.IS_OS_LINUX) {
                //do stuff if linux
                p_unpackLinuxLibraries(jarPath)
                p_loadLinuxLibraries(jarPath)
            }
            p_loadMethods()
        } catch (e: Exception) {
            println(e)
            println("Error loading ffmrand library!")
        }
    }

    private fun p_getlinuxdependencies(): Array<String> {
        return arrayOf(
            "libtss2-sys.so.0.0.0",
            "libtss2-mu.so.0.0.0",
            "libtss2-esys.so.1.0.0",
            "libcrypto.so.3.1.4",
            "libtss2-tctildr.so.0.0.0",
            "libffmrand.so"
        )
    }

    private fun p_getwindowsdependencies(): Array<String> {
        return arrayOf(
            "msvcp140.dll", "vcruntime140.dll", "api-ms-win-crt-heap-l1-1-0.dll",
            "api-ms-win-crt-runtime-l1-1-0.dll", "libcrypto-3-x64.dll",
            "tss2-mu.dll", "tss2-sys.dll", "tss2-tcti-tbs.dll", "tss2-tctildr.dll", "tss2-esys.dll", "ffmrand.dll"
        )
    }

    private fun p_unpackWindowsLibraries(path: String) {
        val libArray = p_getwindowsdependencies()
        val nativeDir = File(path + File.separator + "libjavarandom_native")
        val osDir = File(path + File.separator + "libjavarandom_native" + File.separator + "windows")
        // create temp directories for dlls
        if (!nativeDir.exists()) {
            nativeDir.mkdir()
        }
        if (!osDir.exists()) {
            osDir.mkdir()
        }
        // extract libraries from jar
        for (s in libArray) {
            val link = (KotRandom::class.java.getResourceAsStream("/native/windows/$s"))
            val dllFile =
                File(path + File.separator + "libjavarandom_native" + File.separator + "windows" + File.separator + s)
            try {
                FileUtils.copyInputStreamToFile(link, dllFile)
            } catch (e: Exception) {
                println("error copying dlls")
                return
            }
        }
    }

    private fun p_unpackLinuxLibraries(path: String) {
        val libArray = p_getlinuxdependencies()
        val nativeDir = File(path + File.separator + "libjavarandom_native")
        val osDir = File(path + File.separator + "libjavarandom_native" + File.separator + "linux")
        // create temp directories for dlls
        if (!nativeDir.exists()) {
            nativeDir.mkdir()
        }
        if (!osDir.exists()) {
            osDir.mkdir()
        }

        // extract libraries from jar
        for (s in libArray) {
            val link = (KotRandom::class.java.getResourceAsStream("/native/linux/$s"))
            val dllFile =
                File(path + File.separator + "libjavarandom_native" + File.separator + "linux" + File.separator + s)
            try {
                FileUtils.copyInputStreamToFile(link, dllFile)
            } catch (e: Exception) {
                println("error copying dlls")
                return
            }
        }
    }

    private fun p_loadWindowsLibraries(path: String) {
        val libArray = p_getwindowsdependencies()
        for (i in libArray.indices) {
            println("loading: " + path + File.separator + "libjavarandom_native" + File.separator + "windows" + File.separator + libArray[i])
            System.load(path + File.separator + "libjavarandom_native" + File.separator + "windows" + File.separator + libArray[i])
        }
        lib = SymbolLookup.libraryLookup("ffmrand.dll", allocator)
    }

    private fun p_loadLinuxLibraries(path: String) {
        val libArray = p_getlinuxdependencies()
        for (i in libArray.indices) {
            println("loading: " + path + File.separator + "libjavarandom_native" + File.separator + "linux" + File.separator + libArray[i])
            System.load(path + File.separator + "libjavarandom_native" + File.separator + "linux" + File.separator + libArray[i])
        }
        lib = SymbolLookup.libraryLookup("libffmrand.so", allocator)
    }

    private fun p_loadMethods() {
        init = Linker.nativeLinker().downcallHandle(
            lib.find("p_init").orElseThrow(),

            FunctionDescriptor.of(ValueLayout.JAVA_LONG)
        )
        destroy = Linker.nativeLinker().downcallHandle(
            lib.find("p_destroy").orElseThrow(),

            FunctionDescriptor.of(ValueLayout.JAVA_INT, ValueLayout.JAVA_LONG)
        )
        nextShort = Linker.nativeLinker().downcallHandle(
            lib.find("p_getShort").orElseThrow(),

            FunctionDescriptor.of(ValueLayout.JAVA_SHORT, ValueLayout.JAVA_LONG)
        )
        nextInt = Linker.nativeLinker().downcallHandle(
            lib.find("p_getInt").orElseThrow(),

            FunctionDescriptor.of(ValueLayout.JAVA_INT, ValueLayout.JAVA_LONG)
        )
        nextLong = Linker.nativeLinker().downcallHandle(
            lib.find("p_getLong").orElseThrow(),

            FunctionDescriptor.of(ValueLayout.JAVA_LONG, ValueLayout.JAVA_LONG)
        )
        getRNGQuality = Linker.nativeLinker().downcallHandle(
            lib.find("p_getRNGQuality").orElseThrow(),

            FunctionDescriptor.of(ValueLayout.JAVA_BYTE, ValueLayout.JAVA_LONG)
        )
    }

    @Throws(Throwable::class)
    fun init() {
        pointer = init.invoke() as Long
    }

    @Throws(Throwable::class)
    fun destroy(): Int {
        return destroy.invokeWithArguments(pointer) as Int
    }

    @Throws(Throwable::class)
    fun nextShort(): Short {
        return nextShort.invokeWithArguments(pointer) as Short
    }

    @Throws(Throwable::class)
    fun nextInt(): Int {
        return nextInt.invokeWithArguments(pointer) as Int
    }

    @Throws(Throwable::class)
    fun nextLong(): Long {
        return nextLong.invoke(pointer) as Long
    }

    @get:Throws(Throwable::class)
    val rngQuality: Byte
        get() = getRNGQuality.invokeWithArguments(pointer) as Byte
}