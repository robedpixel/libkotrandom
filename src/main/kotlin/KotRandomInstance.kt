import java.lang.reflect.InvocationTargetException

class KotRandomInstance(private val kotRandomInstance: KotRandomManager) {
    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun nextLong(): Long {
        return kotRandomInstance.nextLong()
    }

    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun nextInt(): Int {
        return kotRandomInstance.nextInt()
    }

    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun nextShort(): Short {
        return kotRandomInstance.nextShort()
    }

    @get:Throws(InvocationTargetException::class, IllegalAccessException::class)
    val rngQuality: Byte
        get() = kotRandomInstance.rngQuality
}