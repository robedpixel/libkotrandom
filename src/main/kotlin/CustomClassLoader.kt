import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

/**
 *
 * Simple custom class loader implementation
 *
 */
class CustomClassLoader() : ClassLoader() {
    /**
     * The HashMap where the classes will be cached
     */
    private val classes: MutableMap<String, Class<*>> = HashMap()

    override fun toString(): String {
        return CustomClassLoader::class.java.name
    }

    @Throws(ClassNotFoundException::class)
    public override fun findClass(name: String): Class<*> {
        if (classes.containsKey(name)) {
            return classes[name]!!
        }

        val classData: ByteArray

        try {
            classData = loadClassData(name)
        } catch (e: IOException) {
            throw ClassNotFoundException(
                "Class [" + name
                        + "] could not be found", e
            )
        }

        val c = defineClass(name, classData, 0, classData.size)
        resolveClass(c)
        classes[name] = c

        return c
    }

    /**
     * Load the class file into byte array
     *
     * @param name
     * The name of the class e.g. com.codeslices.test.TestClass}
     * @return The class file as byte array
     */
    @Throws(IOException::class)
    private fun loadClassData(name: String): ByteArray {
        val `in` = BufferedInputStream(
            Objects.requireNonNull(
                getSystemResourceAsStream(
                    (name.replace(".", "/")
                            + ".class")
                )
            )
        )
        val out = ByteArrayOutputStream()
        var i: Int

        while ((`in`.read().also { i = it }) != -1) {
            out.write(i)
        }

        `in`.close()
        val classData = out.toByteArray()
        out.close()

        return classData
    }
}