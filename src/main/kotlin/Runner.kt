

fun main() {
    val random: KotRandomInstance = KotRandomManager.randomInstance
    println("RNG Grade:" + random.rngQuality)
    println(random.nextInt())
    println(random.nextLong())
    println(random.nextShort())
}
