// Fill out your copyright notice in the Description page of Project Settings.

#ifndef RDRANDRAND_H
#define RDRANDRAND_H

#include "RandEngine.h"

#ifdef _MSC_VER
#include <intrin.h>
#elif defined(__GNUG__)
#include <immintrin.h>
#endif


// TRNG RandEngine class using RDRAND
class RDRANDRand : public RandEngine {

public:
  unsigned int GetRandUShorts(std::vector<uint16_t> *in);
  unsigned int GetRandUInts(std::vector<uint32_t> *in);
  unsigned int GetRandULongs(std::vector<unsigned long long> *in);
private:
  using RandEngine::GetRandUInts;
  using RandEngine::GetRandULongs;
  using RandEngine::GetRandUShorts;
};

#endif // RDRANDRAND_H
