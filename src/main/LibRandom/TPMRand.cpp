// Fill out your copyright notice in the Description page of Project Settings.

#include "TPMRand.h"
// TEST COMMENT
TPMRand::TPMRand() { tpm_err = Esys_Initialize(&tpm_context, nullptr, nullptr); }
TPMRand::~TPMRand() {
  if (tpm_context) {
    Esys_Finalize(&tpm_context);
  }
}
unsigned int TPMRand::GetRandUShorts(std::vector<uint16_t> *in) {
  num_bytes_gen = in->size() * 2;
  // generate random numbers from tpm until buffer is filled
  if (num_bytes_gen <= max_byte_size) {
    Esys_GetRandom(tpm_context, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
                   num_bytes_gen, &random_bytes);
    for (unsigned int i = 0; i < in->size(); i++) {
      in->operator[](i) = static_cast<unsigned int>(
          random_bytes->buffer[2 * i] << 8 | random_bytes->buffer[2 * i + 1]);
    }
    free(random_bytes);
    return 0;
  } else {
    return 1;
  }
}

unsigned int TPMRand::GetRandUInts(std::vector<uint32_t> *in) {
  num_bytes_gen = in->size() * 4;
  if (num_bytes_gen <= max_byte_size) {
    Esys_GetRandom(tpm_context, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
                   num_bytes_gen, &random_bytes);
    for (unsigned int i = 0; i < in->size(); i++) {
      in->operator[](i) = static_cast<unsigned int>(
          static_cast<uint32_t>(random_bytes->buffer[4 * i]) << 24 |
          static_cast<uint32_t>(random_bytes->buffer[4 * i + 1]) << 16 |
          static_cast<uint32_t>(random_bytes->buffer[4 * i + 2]) << 8 |
          static_cast<uint32_t>(random_bytes->buffer[4 * i + 3]));
    }
    free(random_bytes);
    return 0;
  } else {
    return 1;
  }
}
unsigned int TPMRand::GetRandULongs(std::vector<unsigned long long> *in) {
  num_bytes_gen = in->size() * 8;
  if (num_bytes_gen <= max_byte_size) {
    Esys_GetRandom(tpm_context, ESYS_TR_NONE, ESYS_TR_NONE, ESYS_TR_NONE,
                   num_bytes_gen, &random_bytes);
    for (unsigned int i = 0; i < in->size(); i++) {
      in->operator[](i) = static_cast<unsigned long long>(
          static_cast<unsigned long long>(random_bytes->buffer[8 * i]) << 56 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 1]) << 48 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 2]) << 40 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 3]) << 32 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 4]) << 24 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 5]) << 16 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 6]) << 8 |
          static_cast<unsigned long long>(random_bytes->buffer[8 * i + 7]));
    }
    free(random_bytes);
    return 0;
  } else {
    return 1;
  }
}
